Adequate standalone Concourse
=============================

Concourse is a pleasant CI system, but to trial it to its full potential you really want the ability to make your pipeline config public and have secrets injected at runtime.

This means you need to deploy it with an external secret returner. This config is an example of a quick-ish way to use a local Vault instance as that secret returner, while using the `quickstart` command to run a combined master/worker on a single machine.


Steps
-----

```bash
# First, edit the compose.yml file, making up a database password and configuring Github oauth or choosing another method
# Then:

docker-compose -f concourse.compose.yml up -d concourse-db

./create-vault-db.sh
docker-compose -f concourse.compose.yml up -d vault

./init-vault.sh
  # generates a single key and a root token
  # "logs in" with that token inside the container so the ./vault proxy command works

./vault policy write concourse - < concourse.policy.hcl
./vault token create -policy concourse

# Shove that into the compose.yml file. Finally...

docker-compose -f concourse.compose.yml up -d
```

And now it should work. You can add secrets to vault like thus:

```bash
./vault write concourse/main/github_ssh_key_my_project value=@/dev/stdin
# paste the value, then ^D
```

I've definitely forgotten something.
