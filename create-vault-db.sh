#!/bin/bash

mkdir -p ./vault-config
password="$(pwgen -s 48 | tee ./vault-config/db-password.txt)"

cat > ./vault-config/database.hcl <<-EOF
	storage "postgresql" {
	  connection_url = "postgres:$password//vault:@concourse-db:5432/vault?sslmode=disable"
	}
EOF

docker-compose -f ./concourse.compose.yml exec -T concourse-db psql -U postgres <<-EOF
	CREATE DATABASE vault;
	CREATE USER vault WITH PASSWORD '$password';
	GRANT CONNECT ON DATABASE vault TO vault;

	\\connect vault

	CREATE TABLE public.vault_kv_store (
	parent_path TEXT COLLATE "C" NOT NULL,
	path        TEXT COLLATE "C",
	key         TEXT COLLATE "C",
	value       BYTEA,
	CONSTRAINT pkey PRIMARY KEY (path, key)
	);

	GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO vault;

	CREATE INDEX parent_path_idx ON vault_kv_store (parent_path);
EOF
