#!/bin/bash

if ! docker-compose -f ./concourse.compose.yml exec vault vault status; then
	docker-compose -f ./concourse.compose.yml exec vault vault init --key-shares=1 --key-threshold=1 | tee -a ./vault-init.txt
	docker-compose -f ./concourse.compose.yml exec vault vault unseal
fi
docker-compose -f ./concourse.compose.yml exec vault vault secrets enable -path=concourse kv
if ! docker-compose -f ./concourse.compose.yml exec vault vault write /concourse/test hello=world; then
	docker-compose -f ./concourse.compose.yml exec vault vault login
fi
docker-compose -f ./concourse.compose.yml exec vault vault secrets enable -path=concourse kv
